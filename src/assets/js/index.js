

var webRTCLiveStreaming = function () {

  var demo = function () {
    var kurentoIp = 'kurento.lumixanalytics.com', // replace this with your kurento server IP
      // var kurentoIp = '158.69.63.246',
      kurentoPort = '8433', // replace this with your kurento server port
      fileSavePath = 'file:///opt/tomcat/webapps/HireAmigo/'; // replace this with your path & file name

    var args = {
      ws_uri: 'wss://' + kurentoIp + ':' + kurentoPort + '/kurento',
      file_uri: fileSavePath
    };
    var localVideo,
      remoteVideo,
      webRtcPeer,
      client,
      pipeline,
      fullPath;
    var url = $(location).attr('href'),
      parts = url.split("/"),
      userId = parts[parts.length - 2];
    jobId = parts[parts.length - 1];

    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');
    start();
    startInterviewStatus();
    
    function start() {

      var constraints = {
        audio: false,
        video: true
      };

      var options = {
        // localVideo: localVideo,
        // remoteVideo: remoteVideo,
        // onicecandidate : onIceCandidate,
        mediaConstraints: constraints
      };
     
      webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv({
        options,
        localVideo: localVideo,
        remoteVideo: remoteVideo
      }, (error) => {
        if (error) return onError(error);
        webRtcPeer.generateOffer(onStartOffer);

      });
    }

    function stop() {
      if (webRtcPeer) {
        webRtcPeer.dispose();
        webRtcPeer = null;
      }

      if (pipeline) {
        pipeline.release();
        pipeline = null;
      }
    }

    function onStartOffer(error, sdpOffer) {
      if (error) return onError(error);

      co(function* () {
        try {
          if (!client)
            client = yield kurentoClient(args.ws_uri);

          var interviewerName = $('#interviwer_name').val();
          pipeline = yield client.create('MediaPipeline');
          var webRtc = yield pipeline.create('WebRtcEndpoint');
          setIceCandidateCallbacks(webRtcPeer, webRtc, onError);
          var recorder = yield pipeline.create('RecorderEndpoint', { uri: args.file_uri + interviewerName + '/' + userId + ".webm" });
          yield webRtc.connect(recorder);
          yield webRtc.connect(webRtc);
          yield recorder.record();
          var sdpAnswer = yield webRtc.processOffer(sdpOffer);
          webRtc.gatherCandidates(onError);
          webRtcPeer.processAnswer(sdpAnswer);

          // nodeServiceCall();
          // alert(fullPath);
        } catch (e) {
          onError(e);
        }
      })();
    }

    function startInterviewStatus() {
      var update = { jobID: userId, v_path: fullPath, isActive: 'i' };
      $.ajax({
        type: 'POST',
        // url: 'http://localhost:3000/interview/updateInterview',
        // url: 'https://hireamigoservice.lumixanalytics.com:3000/interview/updateInterview',
         url: 'https://services.hireamigo.com:3000/interview/updateInterview',
        data: update,
        success: function (resultData) {
          if (resultData) {

          } else {
            alert("Save Complete Fail..");
          }
        }
      });
    }

    function nodeServiceCall() {
      var interviewerName = $('#interviwer_name').val();
      fullPath = interviewerName + '/' + userId + ".webm";
      // alert(fullPath);
      var update = { userId: userId, v_path: fullPath };
      $.ajax({
        type: 'POST',
        //  url: 'http://localhost:3000/interview/savevideo',
        // url: 'https://hireamigoservice.lumixanalytics.com:3000/interview/savevideo',
         url: 'https://services.hireamigo.com:3000/interview/savevideo',
        data: update,
        success: function (resultData) {
          if (resultData) {
            // alert("Save Complete..");
            var mainurl = window.location.href;
          } else {
            alert("Save Complete Fail..");
          }
        }
      });
    }


    function onError(error) {
      if (error) {
        console.error(error);
        stop();
      }
    }

    function setIceCandidateCallbacks(webRtcPeer, webRtcEp, onerror) {
      webRtcPeer.on('icecandidate', function (candidate) {
        candidate = kurentoClient.getComplexType('IceCandidate')(candidate);
        webRtcEp.addIceCandidate(candidate, onerror);
      });

      webRtcEp.on('OnIceCandidate', function (event) {
        var candidate = event.candidate;
        webRtcPeer.addIceCandidate(candidate, onerror);
      });
    }

    function initBotUI() {

      getQuestionCount();
       var url = $(location).attr('href'),
         parts = url.split("/"),
         userId = parts[parts.length - 2];
       jobId = parts[parts.length - 1];
       // let mediaRecorder;
       // let recordedBlobs;
       // let sourceBuffer;
       var subtitleText;
       var botui = new BotUI('api-bot');
      //  var socket = io.connect('http://localhost:65080/');
       //  var socket = io.connect('https://hireamigoservice.lumixanalytics.com:65080/');
             var socket = io.connect('https://services.hireamigo.com:65080/');
 
 
       var interviewerName = $('#interviwer_name').val();
       var count = 1;
       var totalquestion = 0;
       var startInterviewTime = new Date().getTime();
       var statQuestionTime = new Date().getTime();
       var endQuestionTime = new Date().getTime();
       var questionText = '';
       var subTitleCounter = 1;
 
       var welcomeText = 'Hi, ' + interviewerName + '. I am Hire Amigo from ABAP IT Solutions. I am a virtual recruitment assistant and I will be taking your video interview today. I will ask you one question at a time. You can provide answer after each question. When you have completed answering the question, you can press Next Question button on your screen and I will ask you next question. If you want me to repeat the question then you can press Repeat Question button. Your video interview will be recorded and shared within the company for your evaluation. You will hear from our recruitment team for any further steps after completing this interview. Press Next Question button below to start your interview. Good Luck!';
 
       function stop() {
         if (webRtcPeer) {
           webRtcPeer.dispose();
           webRtcPeer = null;
         }
 
         if (pipeline) {
           pipeline.release();
           pipeline = null;
         }
       }
 
 
 
       function getQuestionCount() {
         // alert('getQuestionCount');
         $.ajax({
           type: 'GET',
          //  url: 'http://localhost:3000/job/getAllQuestionsByJobId/' + jobId,
           // url: 'https://hireamigoservice.lumixanalytics.com:3000/job/getAllQuestionsByJobId/'+jobId,
                     url: 'https://services.hireamigo.com:3000/job/getAllQuestionsByJobId/'+jobId,
 
           success: function (resultData) {
             if (resultData) {
               // alert(resultData.questions.length);
               var data = resultData[0];
               totalquestion = data.questions.length + 1;
               // alert(totalquestion);
               console.log(resultData);
             } else {
               alert("Save Complete Fail..");
             }
           }
         });
 
       }
       botui.message.add({
         /* human: true, */
         content: welcomeText,
         delay: 100,
       }).then(function () {
         texttovoice(welcomeText);
         botui.action.button({
           action: [
             { // show only one button
               text: 'Start',
               value: 'start',
               voices: 'start'
             }
           ]
         }
         ).then(function (res) {
 
           endQuestionTime = new Date().getTime();
           subtitleText = subtitleText + "WEBVTT FILE\n\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " D:vertical A:start\n" + interviewerName + "\n";
           subTitleCounter++;
           socket.emit('fromClient', { client: res.value }); // sends the message typed to server
           nodeServiceCall();
           //texttovoice(res.value);
 
         }).then(
           function () {
             socket.on('fromServer', function (data) { // recieveing a reply from server.
 
               statQuestionTime = new Date().getTime();
               texttovoice(data.server);
               questionText = data.server;
               if (data.server == "Thank you we will get back to you soon.") {
                 setTimeout(1000);
 
               } else {
                 botui.message.add({
                   content: data.server,
                   delay: 100,
                 }).then(function (res) {
                   // alert('In Then ' + totalquestion);
                   if (totalquestion == count) {
                     botui.action.button({
                       action: [
                         { // show only one button
                           text: 'Click to finish',
                           value: 'finish'
                         }
                       ]
                     }
                     ).then(function (res) {
                       endQuestionTime = new Date().getTime();
                       subtitleText = subtitleText + "\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " \n" + questionText + "\n";
                       subTitleCounter++;
                       socket.emit('fromClient', { client: res.value }); // sends the message typed to server
                       //texttovoice(res.value);
                       count++;
                       stop();
                       nodeForFinishInterview();
                       // var mainurl = window.location.href;
                       // var url = mainurl.split('#');
                       // window.location.href = url[0]+'#/thankyou';
                     });
                   } else {
                     botui.action.button({
                       action: [
                         { // show only one button
                           // text: 'Once you finish click here for next question',
                            text: 'Next Question',
                           value: jobId + "-" + count
                         },
                         { // show only one button
                           // text: 'Once you finish click here for next question',
                            text: 'Repeat Question',
                           value: "repeat"
                         }
                       ]
                     }
                     ).then(function (res) {
                       if (res.value === "repeat"){
                         if(count == 1){
                           
                           endQuestionTime = new Date().getTime();
                           subtitleText = subtitleText + "\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " \n" + questionText + "\n";
                           subTitleCounter++;
                           socket.emit('fromClient', { client: "start" }); // sends the message typed to server
                           //texttovoice(res.value);
                           //count++;
                         }else{
                         count --
                         endQuestionTime = new Date().getTime();
                         subtitleText = subtitleText + "\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " \n" + questionText + "\n";
                         subTitleCounter++;
                         socket.emit('fromClient', { client: jobId +"-"+ count }); // sends the message typed to server
                         //texttovoice(res.value);
                         count++;
                         }               
                       }else{
                         endQuestionTime = new Date().getTime();
                         subtitleText = subtitleText + "\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " \n" + questionText + "\n";
                         subTitleCounter++;
                         socket.emit('fromClient', { client: res.value }); // sends the message typed to server
                         //texttovoice(res.value);
                         count++;  
                       } 
                     });
                   }
 
                 })
               }
             });
           })
       });
 
       function nodeForFinishInterview() {
         var update = { jobID: userId, v_path: fullPath, isActive: 'c' };
         $.ajax({
           type: 'POST',
          //  url: 'http://localhost:3000/interview/updateInterview',
           // url: 'https://hireamigoservice.lumixanalytics.com:3000/interview/updateInterview',
                     url: 'https://services.hireamigo.com:3000/interview/updateInterview',
 
           data: update,
           success: function (resultData) {
             if (resultData) {
               var mainurl = window.location.href;
               var url = mainurl.split('#');
               window.location.href = url[0] + '#/thankyou';
             } else {
               alert("Save Complete Fail..");
             }
           }
         });
       }
 
 
     }

    function texttovoice(voiceText) {
      var startSpeechTime = new Date().getTime();
      var synth = window.speechSynthesis;
      var endSpeechTime = 0;
      var text = voiceText;
      var msg = new SpeechSynthesisUtterance();
      var ttt = synth.getVoices();
      var voices = window.speechSynthesis.getVoices(4);
      var interviewerName = $('#interviwer_name').val();
      var str = $('#voices').val();
      msg.voice = voices[str + ''];
      msg.rate = 1;
      msg.pitch = 1;
      msg.text = text;
      msg.lang = "en-GB";
      speechSynthesis.speak(msg);
      msg.onend = function (e) {
        endSpeechTime = new Date().getTime();
      };

      return endSpeechTime - startSpeechTime;
    }

    function convertWebVttFormat(inputTime) {
      var distance = inputTime;
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = ("0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2);
      var minutes = ("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2);
      var seconds = ("0" + Math.floor((distance % (1000 * 60)) / 1000)).slice(-2);
      var miliseconds = ("00" + Math.floor((distance % (1000)))).slice(-3);

      // Output the result in an element with id="demo"
      var finalCounter = hours + ":"
        + minutes + ":" + seconds + "." + miliseconds;
      return finalCounter;
    }
    initBotUI();

    

  };

  return {
    // public functions
    init: function () {
      demo();
    },
  };

}();


// var initBot = function () {
//   var demo2 = function() {

//       function initBotUI() {
//         var url = $(location).attr('href'),
//         parts = url.split("/"),
//         userId = parts[parts.length - 1];
//         // let mediaRecorder;
//           // let recordedBlobs;
//           // let sourceBuffer;
//         var subtitleText;
//         var botui = new BotUI('api-bot');
//         var socket = io.connect('http://localhost:65080/');
//         // var socket = io.connect('https://hireamigoservice.lumixanalytics.com:65080/');

//         var interviewerName = $('#interviwer_name').val();
//         var count = 1;
//         var totalquestion = 4;
//         var startInterviewTime = new Date().getTime();
//         var statQuestionTime = new Date().getTime();
//         var endQuestionTime = new Date().getTime();
//         var questionText = '';
//         var subTitleCounter = 1;

//         var welcomeText = 'Hey ' + interviewerName + '. Welcome to virtual interview Room, Click start when you ready for the interview.';

//         function stop() {
//           if (webRtcPeer) {
//               webRtcPeer.dispose();
//               webRtcPeer = null;
//           }

//           if (pipeline) {
//               pipeline.release();
//               pipeline = null;
//           }
//       }

//         botui.message.add({
//           /* human: true, */
//           content: welcomeText,
//           delay: 100,
//         }).then(function () {
//           texttovoice(welcomeText);
//           botui.action.button({
//             action: [
//               { // show only one button
//                 text: 'Start',
//                 value: 'start',
//                 voices: 'start'
//               }
//             ]
//           }
//           ).then(function (res) {

//             endQuestionTime = new Date().getTime();
//             subtitleText = subtitleText + "WEBVTT FILE\n\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " D:vertical A:start\n" + interviewerName + "\n";
//             subTitleCounter++;
//             socket.emit('fromClient', { client: res.value }); // sends the message typed to server
//             //texttovoice(res.value);

//           }).then(
//             function () {
//               socket.on('fromServer', function (data) { // recieveing a reply from server.

//                 statQuestionTime = new Date().getTime();
//                 texttovoice(data.server);
//                 questionText = data.server;
//                 if (data.server == "Thank you we will get back to you soon.") {
//                   setTimeout(1000);

//                 } else {
//                   botui.message.add({
//                     content: data.server,
//                     delay: 100,
//                   }).then(function (res) {
//                     if (totalquestion == count) {
//                       botui.action.button({
//                         action: [
//                           { // show only one button
//                             text: 'Click to finish',
//                             value: 'finish'
//                           }
//                         ]
//                       }
//                       ).then(function (res) {
//                         endQuestionTime = new Date().getTime();
//                         subtitleText = subtitleText + "\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " \n" + questionText + "\n";
//                         subTitleCounter++;
//                         socket.emit('fromClient', { client: res.value }); // sends the message typed to server
//                         //texttovoice(res.value);
//                         count++;
//                         stop();
//                         nodeForFinishInterview();
//                         // var mainurl = window.location.href;
//                         // var url = mainurl.split('#');
//                         // window.location.href = url[0]+'#/thankyou';
//                       });
//                     } else {
//                       botui.action.button({
//                         action: [
//                           { // show only one button
//                             text: 'Once you finish click here for next question',
//                             value: 'question-' + count
//                           }
//                         ]
//                       }
//                       ).then(function (res) {
//                         endQuestionTime = new Date().getTime();
//                         subtitleText = subtitleText + "\n" + subTitleCounter + "\n" + convertWebVttFormat(statQuestionTime - startInterviewTime) + " --> " + convertWebVttFormat(endQuestionTime - startInterviewTime) + " \n" + questionText + "\n";
//                         subTitleCounter++;
//                         socket.emit('fromClient', { client: res.value }); // sends the message typed to server
//                         //texttovoice(res.value);
//                         count++;
//                       });
//                     }

//                   })
//                 }
//               });
//             })
//         });

//         function nodeForFinishInterview(){
//           var update = { userId: userId, v_path: fullPath };
//           $.ajax({
//             type: 'POST',
//             url: 'http://localhost:3000/interview/finishInterview',
//             // url: 'https://hireamigoservice.lumixanalytics.com:3000/interview/savevideo',
//             data: update,
//             success: function(resultData) {
//               if (resultData) {
//                 var mainurl = window.location.href;
//                 var url = mainurl.split('#');
//                 window.location.href = url[0]+'#/thankyou';      
//               } else {
//                 //alert("Save Complete Fail..");
//               }
//             }
//           });
//         }
//       }

//       function texttovoice(voiceText) {
//         var startSpeechTime = new Date().getTime();
//         var synth = window.speechSynthesis;
//         var endSpeechTime = 0;
//         var text = voiceText;
//         var msg = new SpeechSynthesisUtterance();
//         var ttt = synth.getVoices();
//         var voices = window.speechSynthesis.getVoices(4);
//         var interviewerName = $('#interviwer_name').val();
//         var str = $('#voices').val();
//         msg.voice = voices[str + ''];
//         msg.rate = 1;
//         msg.pitch = 1;
//         msg.text = text;
//         msg.lang = "en-GB";
//         speechSynthesis.speak(msg);
//         msg.onend = function (e) {
//           endSpeechTime = new Date().getTime();
//         };

//         return endSpeechTime - startSpeechTime;
//       }

//       function convertWebVttFormat(inputTime) {
//         var distance = inputTime;
//         var days = Math.floor(distance / (1000 * 60 * 60 * 24));
//         var hours = ("0" + Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))).slice(-2);
//         var minutes = ("0" + Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))).slice(-2);
//         var seconds = ("0" + Math.floor((distance % (1000 * 60)) / 1000)).slice(-2);
//         var miliseconds = ("00" + Math.floor((distance % (1000)))).slice(-3);

//         // Output the result in an element with id="demo"
//         var finalCounter = hours + ":"
//           + minutes + ":" + seconds + "." + miliseconds;
//         return finalCounter;
//       }
//       initBotUI();
//   };

// return {
//   // public functions
//   init: function() {
//       demo2();
//   },
// };

// }();

jQuery(document).ready(function () {

  if ($('textarea#ta').length) {
    CKEDITOR.replace('ta');
  }

  $('a.confirmDeletion').on('click', function () {
    if (!confirm('Confirm deletion'))
      return false;
  });

  $('a.confirmDeletion').on('click', function () {
    if (!confirm('Confirm deletion'))
      return false;
  });

  $('#startInterview').on('click', function () {
    if (!confirm('Are you ready for video interview? please check your mic is on'))
      return false;

    $('#interviwer_name').attr('disabled', 'true');
    $('#interviwer_email').attr('disabled', 'true');
    $("#startInterview").hide();

    $('.recordVideo').html('<div class="row"><div class="col-md-6"><video class ="mob-video"style ="border-radius:10px; box-shadow: 5px 10px 18px #888888;"id="localVideo" autoplay playsinline muted></video></div><div class="col-md-6"><div class="botui-app-container" id="api-bot"><bot-ui></bot-ui></div></div></div>');
    webRTCLiveStreaming.init();
    //initBot.init();

  });

});

