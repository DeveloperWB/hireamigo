import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from "./auth/logout/logout.component";
import { StartInterviewsComponent } from './auth/start-interview/start-interview.component';
import { ThankyouComponent } from './auth/thankyou/thankyou.component';
import { PagenotfoundComponent } from './auth/pagenotfound/pagenotfound.component';
import { RoleGuardService } from './auth/_guards/roleGuardService';


export const routes: Routes = [
  
  { path: 'login',
   loadChildren: './auth/auth.module#AuthModule' 
  },
  { path: 'logout',
   component: LogoutComponent
   },
  { path: '', redirectTo: 'dashboard', 
  pathMatch: 'full' 
  },
  { path: 'start-interview/:id/:jobId',
   component: StartInterviewsComponent ,
  },
  { path: 'thankyou',
   component: ThankyouComponent 
  },
  { path: 'pagenotfound',
   component: PagenotfoundComponent 
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
