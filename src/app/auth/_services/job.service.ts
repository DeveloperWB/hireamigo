import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../../../app.config';


@Injectable()
export class JobService {
  constructor(private http: HttpClient) { }

  getAllJobs() {
    return this.http.get<any>(appConfig.apiUrl + '/job/getAlljobs')
      .map(res => {
        return res;
      });
  }

  getAlldepartment() {
    return this.http.get<any>(appConfig.apiUrl + '/job/getAlldepartment')
      .map(res => {
        return res;
      });
  }

  deleteJob(job) {
    return this.http.post<any>(appConfig.apiUrl + '/job/delete', job)
      .map(res => {
        return res;
      });
  }

  addNewJob(job) {
    return this.http.post<any>(appConfig.apiUrl + '/job/addNewJob', job)
      .map(res => {
        return res;
      });
  }

  updateJob(job) {
    return this.http.post<any>(appConfig.apiUrl + '/job/updateJob', job)
      .map(res => {
        return res;
      });
  }

  activeInactiveJob(job){
    return this.http.post<any>(appConfig.apiUrl+'/job/activeInactiveJob', job)
    .map(res => {
        return res;
    });
}

openstatus(job) {
  return this.http.get<any>(appConfig.apiUrl + '/job/getAllStatus/' + job)
      .map(res => {
          return res;
      });
}

closestatus(job) {
  return this.http.get<any>(appConfig.apiUrl + '/job/getAllStatusFalse/' + job)
      .map(res => {
          return res;
      });
}

allstatus() {
  return this.http.get<any>(appConfig.apiUrl + '/job/getAlljobsData/')
      .map(res => {
          return res;
      });
}

}
