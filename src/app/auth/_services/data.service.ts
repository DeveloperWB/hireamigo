import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/Rx';
import {DayPilot} from 'daypilot-pro-angular';

@Injectable()
export class DataService {

  tasks: any[] = [
    {
      id: "1",
      text: "Task 1",
      column: "1",
      barColor: "#F6B26B"
    },
    {
      id: "2",
      text: "Task 2",
      column: "1",
      barColor: "#6AA84F"
    },
    {
      id: "3",
      text: "Task 3",
      column: "2"
    },
  ];

  columns: any[] = [
    {
      name: "Phase 1",
      id: "1"
    },
    {
      name: "Phase 2",
      id: "2"
    },
    {
      name: "Phase 3",
      id: "3"
    }
  ];

  constructor(private http : Http){
  }

  getCards(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.tasks);
      }, 200);
    });

    // return this.http.get("/api/cards").map((response:Response) => response.json());
  }

  getColumns(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.columns);
      }, 200);
    });

    // return this.http.get("/api/columns").map((response:Response) => response.json());
  }

}
