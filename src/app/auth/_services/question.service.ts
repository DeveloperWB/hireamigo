import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../../../app.config';


@Injectable()
export class QuestionService {
  constructor(private http: HttpClient) { }

  getQuestionsByJobId(){
    return this.http.get<any>(appConfig.apiUrl + '/question/getAllQuestionsByJobId')
    .map(res => {
      return res;
    });
  }

  getAllQuestions() {
    return this.http.get<any>(appConfig.apiUrl + '/question/getAllQuestions')
      .map(res => {
        return res;
      });
  }

  deleteQuestion(question) {
    return this.http.post<any>(appConfig.apiUrl + '/question/delete', question)
      .map(res => {
        return res;
      });
  }

  addNewQuestion(question) {
    return this.http.post<any>(appConfig.apiUrl + '/question/addNewQuestion', question)
      .map(res => {
        return res;
      });
  }

  getAlldepartment() {
    return this.http.get<any>(appConfig.apiUrl + '/job/getAlldepartment')
      .map(res => {
        return res;
      });
  }

  createQuestion(question) {
    return this.http.post<any>(appConfig.apiUrl + '/question/createQuestion', question)
      .map(res => {
        return res;
      });
  }

  updateQuestion(question) {
    return this.http.post<any>(appConfig.apiUrl + '/question/updateQuestion', question)
      .map(res => {
        return res;
      });
  }

  addMultipleQuestion(question) {
    return this.http.post<any>(appConfig.apiUrl + '/question/addMultipleQuestion', question)
      .map(res => {
        return res;
      });
  }

  editQuestion(question) {
    return this.http.get<any>(appConfig.apiUrl + '/question/editQuestion/' + question._id)
      .map(res => {
        return res;
      })
  }

  // addNewQuestiondialogflow(question) {
  //   return this.http.get<any>(appConfig.apiUrl + '/addNewQuestion', question)
  //     .map(res => {
  //       return res;
  //     });
  // }

}
