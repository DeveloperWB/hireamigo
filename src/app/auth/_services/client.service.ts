import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../../../app.config';

@Injectable()
export class ClientService {
  constructor(private http: HttpClient) { }


  getAllClients() {
    return this.http.get<any>(appConfig.apiUrl + '/client/getAllClients')
      .map(res => {
        return res;
      });
  }

  updateClient(client) {
    return this.http.post<any>(appConfig.apiUrl + '/client/updateClient', client)
      .map(res => {
        return res;
      });
  }

  deleteClient(client) {
    return this.http.post<any>(appConfig.apiUrl + '/client/delete', client)
      .map(res => {
        return res;
      });
  }

  addNewClient(client) {
    return this.http.post<any>(appConfig.apiUrl + '/client/addNewClient', client)
      .map(res => {
        return res;
      });
  }


}
