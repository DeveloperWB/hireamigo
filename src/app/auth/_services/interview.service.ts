import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";

import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../../../app.config';


@Injectable()
export class InterviewService {
  constructor(private http: HttpClient) { }

  resendEmailInterviews(interview){
    return this.http.post<any>(appConfig.apiUrl + '/interview/resendEmail', interview)
    .map(res => {
      return res;
    });
  }

  getAllInterviews() {
    return this.http.get<any>(appConfig.apiUrl + '/interview/getAllinterviews')
      .map(res => {
        return res;
      });
  }

  deleteInterview(interview) {
    return this.http.post<any>(appConfig.apiUrl + '/interview/delete', interview)
      .map(res => {
        return res;
      });
  }

  addNewInterview(interview) {
    return this.http.post<any>(appConfig.apiUrl + '/interview/addNewInterview', interview)
      .map(res => {
        return res;
      });
  }

  addMultipleInterviews(interview) {
    return this.http.post<any>(appConfig.apiUrl + '/interview/addMultipleInterviews', interview)
      .map(res => {
        return res;
      });
  }

  verifyinterview(interviewId) {
    return this.http.get<any>(appConfig.apiUrl + '/interview/interviewExpire/' + interviewId)
      .map(res => {
        return res;
      });
  }

  update(interview) {
    return this.http.post<any>(appConfig.apiUrl + '/interview/updateInterview', interview)
      .map(res => {
        return res;
      })
  }



}
