import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../../../app.config';

@Injectable()
export class EmailtempService {
  constructor(private http: HttpClient) { }


  getAllTemplate() {
    return this.http.get<any>(appConfig.apiUrl + '/emailtemp/getAllTemplate')
      .map(res => {
        return res;
      });
  }

  updateEmailtemp(emailtemp) {
    return this.http.post<any>(appConfig.apiUrl + '/emailtemp/updateEmailtemp', emailtemp)
      .map(res => {
        return res;
      });
  }

  delete(emailtemp) {
    return this.http.post<any>(appConfig.apiUrl + '/emailtemp/delete',emailtemp)
      .map(res => {
        return res;
      });
  }

  addEmailtemp(emailtemp) {
    return this.http.post<any>(appConfig.apiUrl + '/emailtemp/addEmailtemp', emailtemp)
      .map(res => {
        return res;
      });
  }


}
