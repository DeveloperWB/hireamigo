import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import { map } from "rxjs/operators";
import 'rxjs/add/operator/map';
// import { JwtHelperService } from '@auth0/angular-jwt';
import { appConfig } from '../../../app.config';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) { }

  login(username: string, password: string, companyId: string) {
    return this.http.post<any>(appConfig.apiUrl + '/auth/login', { username: username, password: password, companyId: companyId})   
    .map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.token) {
          // console.log(user);
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('userType', JSON.stringify(user.user.userType));
          localStorage.setItem('token', JSON.stringify(user.token));
          localStorage.setItem('username', user.user.userID);
          localStorage.setItem('companyId', user.user.companyId);



        }
        return user;
      });
  }
  
//   public isAuthenticated(): boolean {

//     const token = localStorage.getItem('token');

   
//     const helper = new JwtHelperService();
//     console.log(!helper.isTokenExpired(token));
//     return !helper.isTokenExpired(token);
// }

public isLogout (){
  localStorage.removeItem('token');
  localStorage.clear();
}
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('token');
  }
}
