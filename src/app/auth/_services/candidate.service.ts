import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { appConfig } from '../../../app.config';

@Injectable()
export class CandidateService {
  constructor(private http: HttpClient) { }


  getAllCandidate() {
    return this.http.get<any>(appConfig.apiUrl + '/candidate/getAllCandidate')
      .map(res => {
        return res;
      });
  }

  updateCandidate(candidate) {
    return this.http.post<any>(appConfig.apiUrl + '/candidate/updateCandidate', candidate)
      .map(res => {
        return res;
      });
  }

  deleteCandidate(candidate) {
    return this.http.post<any>(appConfig.apiUrl + '/candidate/delete',candidate)
      .map(res => {
        return res;
      });
  }

  addNewCandidate(candidate) {
    return this.http.post<any>(appConfig.apiUrl + '/candidate/addCandidate', candidate)
      .map(res => {
        return res;
      });
  }

  getAllCandidateEmail() {
    return this.http.get<any>(appConfig.apiUrl + '/candidate/getAllCandidateEmail')
      .map(res => {
        return res;
      });
  }

}
