import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "../_services/authentication.service";
declare var MediaStreamRecorder: any;
declare var MediaRecorderWrapper: any;
declare var WhammyRecorder: any;
//  import * as $ from 'jquery';
import * as $ from 'jquery';
import { ScriptLoaderService } from "../../_services/script-loader.service";
import { InterviewService } from '../_services/index.1';
declare var CKEDITOR: any;
declare var MediaRecorder: any;
declare var io: any;
declare var window: any;
declare var BotUI: any;
declare var voices: any;
declare var kurentoUtils: any;
declare var kurentoClient: any;
declare var co: any;
// import * as index from '../../../assets/demo/default/custom/components/index.js';

@Component({
  selector: 'app-interview-start',
  templateUrl: './start-interview.html',
  encapsulation: ViewEncapsulation.None,
})

export class StartInterviewsComponent implements OnInit, AfterViewInit {

  constructor(private _router: Router,
    private activatedRoute: ActivatedRoute,
    private _script: ScriptLoaderService,
    public interviewService: InterviewService, ) {
  }
  interview: any = {};
  interviewId = '';
  args;
  loading = false;

  ngOnInit(): void {

    this.interviewId = this._router.url.split('/')[2];
    var jobId = this._router.url.split('/')[3];
    //  this.getValidateInterview(jobId);
    // var fileSavePath = 'file:///tmp/abc.webm';
    // this.args = {
    //   ws_uri: 'ws://kurento.lumixanalytics.com:8433/kurento',
    //   file_uri: fileSavePath
    // };
    this.verifyinterview(this.interviewId);
  }

  verifyinterview(interviewId) {
    this.interviewService.verifyinterview(interviewId)
      .subscribe(
        data => {
          if (data.verified != 0) {
            console.log(data);
            console.log('verified');
          }
          else {
            this._router.navigate(['/pagenotfound']);
          }
          this.interview = data;
        },
        error => {
          this.loading = false;
        });
  }

  ngAfterViewInit() {
  }
} 
