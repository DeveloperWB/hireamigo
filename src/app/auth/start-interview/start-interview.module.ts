import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BaseRequestOptions, HttpModule } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { StartInterviewsComponent } from './start-interview.component';


@NgModule({
  declarations: [
    StartInterviewsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpModule 
  ],
  providers: [
  ],
  entryComponents: [StartInterviewsComponent],
})

export class StartInterviewModule {
}
