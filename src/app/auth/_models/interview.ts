export class User {
  id: number;
  emailid: string;
  skypeid: string;
  cellnumber: number;
  weblink: string;
  status: string;

}
