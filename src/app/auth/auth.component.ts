import {
  Component,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ScriptLoaderService } from '../_services/script-loader.service';
import { AuthenticationService } from './_services/authentication.service';
import { AlertService } from './_services/alert.service';
import { UserService } from './_services/user.service';
import { ClientService } from './_services/client.service';
import { AlertComponent } from './_directives/alert.component';
import { LoginCustom } from './_helpers/login-custom';
import { Helpers } from '../helpers';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';
@Component({
  selector: '.m-grid.m-grid--hor.m-grid--root.m-page',
  templateUrl: './templates/login-3.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class AuthComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  usertypes = [{name:'Client Admin', value:1},{name:'Manager',value:2}, {name:'Team Lead',value:3}];

  @ViewChild('alertSignin',
    { read: ViewContainerRef }) alertSignin: ViewContainerRef;
  @ViewChild('alertSignup',
    { read: ViewContainerRef }) alertSignup: ViewContainerRef;
  @ViewChild('alertForgotPass',
    { read: ViewContainerRef }) alertForgotPass: ViewContainerRef;
    
    private toasterService: ToasterService;
    public toasterconfig : ToasterConfig =
    new ToasterConfig({
      tapToDismiss: true,
      timeout: 5000
    });
  constructor(
    private _router: Router,
    private _script: ScriptLoaderService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _authService: AuthenticationService,
    private _alertService: AlertService,
    private cfr: ComponentFactoryResolver,toasterService: ToasterService) {
      this.toasterService = toasterService;
  }
  showWarning() {
  }

  ngOnInit() {
    this.model.remember = true;
    // get return url from route parameters or default to '/'
    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
    this._router.navigate([this.returnUrl]);

    this._script.loadScripts('body', [
      'assets/vendors/base/vendors.bundle.js',
      'assets/demo/default/base/scripts.bundle.js'], true).then(() => {
        Helpers.setLoading(false);
        LoginCustom.init();
      });
  }



  signin() {
    this.loading = true;
    this._authService.login(this.model.username, this.model.password, this.model.companyId)
    .subscribe(
      data =>{
      if (data.token){
        // console.log(data);  
        if( data.user.userType == 3){
          this._router.navigate(['/components/interviewinvite']);    
        }else{
          this._router.navigate([this.returnUrl]);   
        }          
       }else{
         this._router.navigate(['/login']); 
       }},
      error => {
        // this.showAlert('alertSignin');
        this.toasterService.pop('error', 'Unathorised', 'Username Or Password Invalid');
        this.loading = false;
      });
  }

  signup() {
    this.loading = true;
    this._userService.register(this.model).subscribe(
      data => {
        this.showAlert('alertSignin');
        this._alertService.success(
          'Thank you. To complete your registration.',
          true);
        this.loading = false;
        LoginCustom.displaySignInForm();
        this.model = {};
      },
      error => {
        this.showAlert('alertSignup');
        this._alertService.error(error);
        this.loading = false;
      });
  }


  getLoggedInUser(){
    this._userService.getLoggedInUser()
    .subscribe(
        data => {
          var loggedInUser = data.authData.user;
          // console.log(data.authData.user);
          localStorage.setItem('userType', loggedInUser.userType);
        },
        error => {            
        });
  }

  // forgotPass() {
  //     this.loading = true;
  //     this._userService.forgotPassword(this.model.email).subscribe(
  //         data => {
  //             this.showAlert('alertSignin');
  //             this._alertService.success(
  //                 'Cool! Password recovery instruction has been sent to your email.',
  //                 true);
  //             this.loading = false;
  //             LoginCustom.displaySignInForm();
  //             this.model = {};
  //         },
  //         error => {
  //             this.showAlert('alertForgotPass');
  //             this._alertService.error(error);
  //             this.loading = false;
  //         });
  // }

  showAlert(target) {
    this[target].clear();
    let factory = this.cfr.resolveComponentFactory(AlertComponent);
    let ref = this[target].createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }
}
