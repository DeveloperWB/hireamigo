import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "../_services/authentication.service";
//  import * as $ from 'jquery';
import * as $ from 'jquery';
import { ScriptLoaderService } from "../../_services/script-loader.service";


@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.html',
  encapsulation: ViewEncapsulation.None,
})

export class ThankyouComponent implements OnInit, AfterViewInit {

  constructor(private _router: Router,
    private activatedRoute: ActivatedRoute,
    private _script: ScriptLoaderService) {

  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {

  }

}
