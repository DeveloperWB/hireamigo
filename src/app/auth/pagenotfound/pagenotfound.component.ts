import { Component, OnInit, ViewEncapsulation, AfterViewInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ScriptLoaderService } from "../../_services/script-loader.service";

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class PagenotfoundComponent implements OnInit, AfterViewInit {

  constructor(private _router: Router,
    private activatedRoute: ActivatedRoute,
    private _script: ScriptLoaderService) {
  }
 

  ngOnInit(){}

  ngAfterViewInit() {

  }
} 
