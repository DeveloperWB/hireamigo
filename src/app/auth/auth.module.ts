import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BaseRequestOptions, HttpModule } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { AuthRoutingModule } from './auth-routing.routing';
import { AuthComponent } from './auth.component';
import { AlertComponent } from './_directives/alert.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGuard } from './_guards/auth.guard';
import { AlertService } from './_services/alert.service';
import { AuthenticationService } from './_services/authentication.service';
import { UserService } from './_services/user.service';
import { InterviewService } from './_services/interview.service';
import { ClientService } from './_services/client.service';
import { RecaptchaModule } from 'ng-recaptcha';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';
import { fakeBackendProvider } from './_helpers';
import { SignupEqualValidator } from './_services/signupPasswordValidator';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AuthComponent,
    AlertComponent,
    LogoutComponent,
    SignupEqualValidator
  ],
  imports: [
    CommonModule,
    FormsModule,
    // HttpModule,
    HttpClientModule,
    AuthRoutingModule,
    ToasterModule,
    RecaptchaModule.forRoot(),

  ],
  providers: [
    // AuthGuard,
    AlertService,
    InterviewService,
    // AuthenticationService,
    // UserService,
    // ClientService,
    // api backend simulation
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions,
  ],
  entryComponents: [AlertComponent],
})

export class AuthModule {
}
