import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {DayPilot} from "daypilot-pro-angular";
import {HttpClient} from "@angular/common/http";
import { InterviewService } from '../../../../auth/_services/interview.service';



@Injectable()
export class DataService {
  // for (var i = 0; i <=result.length  i++) {

  // }

  cards: any[] = [
    {id: 1, "name": "Task 1", column: "1", text: "This is a description of task #1."},
    {id: 2, "name": "Task 2", column: "3", text: "This is a description of task #2.", barColor: "#6AA84F"},
    {id: 3, "name": "Task 3", column: "2", text: "This is a description of task #3.", barColor: "#F6B26B"},
    {id: 4, "name": "Task 4", column: "3", text: "This is a description of task #4.", barColor: "#6AA84F"},
    {id: 5, "name": "Task 5", column: "2", text: "This is a description of task #5.", barColor: "#F6B26B"},
    {id: 6, "name": "Task 6", column: "3", text: "This is a description of task #6.", barColor: "#6AA84F"},
  ];

  columns: any[] = [
    {name: "Pending", id: "1"},
    {name: "In Progress", id: "2"},
    {name: "Completed", id: "3"},
    {name: "On Hold", id: "4"},
    {name: "Rejected", id: "5"}
  ];
  
  firstOfMonth(): DayPilot.Date {
    return DayPilot.Date.today().firstDayOfMonth();
  }
  interviews: any[];
  loading = false;
  constructor(private http : HttpClient,public interviewService: InterviewService){
   
  }

  getCards(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.cards);
      }, 200);
    });

    // return this.http.get("/api/tasks");
  }

 
  getColumns(): Observable<any[]> {

    // simulating an HTTP request
    return new Observable(observer => {
      setTimeout(() => {
        observer.next(this.columns);
      }, 200);
    });
  }
}
