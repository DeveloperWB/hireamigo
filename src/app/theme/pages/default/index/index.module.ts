import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index.component';
import { LayoutModule } from '../../../layouts/layout.module';
import { DefaultComponent } from '../default.component';
import { SchedulerModule } from '@progress/kendo-angular-scheduler';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { CalendarModule } from 'angular-calendar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataService } from './data.service';
import { DayPilotModule } from 'daypilot-pro-angular';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { DatePipe } from '@angular/common';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';
import { DataTableModule } from 'angular2-datatable';

// import { AccordionModule } from '@syncfusion/ej2-angular-navigations';
// import { CollapseModule, WavesModule } from 'angular-bootstrap-md';
const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": IndexComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,NgbModule.forRoot(), HttpModule, RouterModule.forChild(routes), LayoutModule, SchedulerModule, DateInputsModule,DayPilotModule,
    ButtonsModule, FlatpickrModule.forRoot(), CalendarModule.forRoot(), ReactiveFormsModule, FormsModule,
    NgSelectModule,ToasterModule,DataTableModule

  ], exports: [
    RouterModule
  ], declarations: [
    IndexComponent, DataFilterPipe
  ],
  providers: [DataService, DatePipe] 
})
export class IndexModule {

}
