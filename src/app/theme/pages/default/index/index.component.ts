import { Component, OnInit, ViewEncapsulation, AfterViewInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ScriptLoaderService } from '../../../../_services/script-loader.service';
import { IndexModule } from './index.module';
import { UserService, InterviewService, JobService, CandidateService, QuestionService } from '../../../../auth/_services/index.1';
import { SchedulerEvent, EditMode, CreateFormGroupArgs } from '@progress/kendo-angular-scheduler';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { DayPilotKanbanComponent } from 'daypilot-pro-angular';
import { DatePipe } from '@angular/common';
import { DataService } from './data.service'; import { Column } from 'primeng/primeng';
// import { appConfig } from 'src/app.config';
import { appConfig } from '../../../../../app.config';
import { IOption } from 'ng-select';
import { ToasterModule, ToasterService, ToasterConfig } from 'angular2-toaster/angular2-toaster';

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  styleUrls: ['./index.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class IndexComponent implements OnInit, AfterViewInit {

  public data;
  candidates: any;
  job: any = {};
  public jobstatus;
  jobs : any;
  users: any;
  JobFlag = false;
  public candidatelist;
  interviewslen = 0;
  // jobs = 0;
  public formGroup: FormGroup;
  public events: SchedulerEvent[] = []
  public interviewlist;
  public active = false;
  // public questions;  
  public selectedDate: Date = new Date();
  config: any = [];
  public isCollapsed = false;
  @Output()
  public cancel: EventEmitter<any> = new EventEmitter();
  new_Interview = 0;
  interview: any = {};
  openjobs = 0;
  interviews: any[];
  loading = false;
  questionlist: any = [];
  questions: any = [];
  question: any = {};
  public ap: boolean = false;
  public dept: Array<IOption> = [];
  public dom: Array<IOption> = [];
  public departmentlist;
  public domainlist;
  public jobList;
  questionObj: any = {};
  departments = ['Technology', 'HR'];
  domains = ['Introductory', 'Personal', 'Behavioral', 'Technical', ' Aptitude'];

  // @ViewChild("kanban")
  // kanban: DayPilotKanbanComponent;
  @Output()
  public save: EventEmitter<any> = new EventEmitter();
  private toasterService: ToasterService;
  public toasterconfig: ToasterConfig =
    new ToasterConfig({
      tapToDismiss: true,
      timeout: 5000
    });

  questionjob;
  constructor(private _script: ScriptLoaderService,
    private userService: UserService,
    public candidateService: CandidateService,
    private interviewService: InterviewService,
    public questionService: QuestionService,
    private jobService: JobService,
    private formBuilder: FormBuilder,
    private ds: DataService,
    private datePipe: DatePipe,
    toasterService: ToasterService) {
    this.toasterService = toasterService;
    // this.createFormGroup = this.createFormGroup.bind(this);
    this.getAllInterviews();
    this.getAllJobs();
    this.getAllQuestion();
    this.getAllCandidate();
    this.getAllscheduler();
    //this.getKanbanData();
    this.createdateModal(this.job);
    this.ds.getColumns().subscribe(result => {
      this.config.columns = result;
    });
  }
  ngOnInit() {
  }
  candidateEmails = [];

  onAddEmail($event) {
    this.candidateEmails.push($event);
  }

  onRemoveEmail($event) {
    this.candidateEmails.splice($event, 1);
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-index',
      ['assets/demo/default/custom/components/base/treeview.js']);
  }

  public getNextId(): number {
    const len = this.events.length;

    return (len === 0) ? 1 : this.events[this.events.length - 1].id + 1;
  }

  public onCancel(e: MouseEvent): void {
    e.preventDefault();
    this.active = false;

    this.cancel.emit();
  }

  resendEmail(interview) {
    this.interviewService.resendEmailInterviews(interview)
      .subscribe(
        data => {
          alert("Re-Send Email Successfully");
          this.getAllInterviews();
        },
        error => {
          this.loading = false;
        });
  }

  showLive(interview) {
    var fullpath = "http://158.69.63.246:8080/HireAmigo/" + interview.videoPath;
    this.interview = interview;
    this.interview.videoPath = fullpath;
  }

  // getAllInterviews() {
  //   this.interviewService.getAllInterviews()
  //     .subscribe(
  //       data => {
  //         var result = data;

  //         if (result.length > 0) {
  //           var nData = [];
  //           result.forEach(element => {
  //             if (element.videoPath != null) {
  //               var path = element.videoPath;
  //               var fullpath = "http://158.69.63.246:8080/HireAmigo/" + path;
  //               element.videoPath = fullpath;
  //               nData.push(element);
  //             } else {
  //               nData.push(element);
  //             }
  //             this.interviews = nData;
  //           });
  //         } else {
  //           this.interviews = [];
  //         }
  //       },
  //       error => {
  //         this.loading = false;
  //       });
  // }

  completedCount(job) {
    var interviews = job.interviewIds;
    var data = interviews.filter(x => x.isActive == 'c');
    return data.length;
  }

  pendingCount(job) {
    var interviews = job.interviewIds;
    var data = interviews.filter(x => x.isActive == 'p');
    return data.length;
  }

  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(
        data => {
          this.users = data.length;
        },
        error => {
        });
  }
  
  getAllInterviews() {
    this.interviewService.getAllInterviews()
      .subscribe(
      data => {
        this.interviews = data;
        if (data.length > 0){
          this.interviews = data.length;
          var count = 0;
          data.forEach(element => {
            if(element.isActive == 'p'){
              count++;
            }
          });
          this.new_Interview = count;
        }
      },
      error => {
        this.loading = false;

      });
  }

  getAllCandidate() {
    this.candidateService.getAllCandidate()
      .subscribe(
        data => {
          this.candidates = data.length;
          this.candidatelist = data;
        },
        err => {
          console.log(err);
        }
      )
  }

  getAllJobs() {
    this.jobService.getAllJobs()
      .subscribe(
        data => {
          this.jobList = data;
          if (data.length > 0) {
            this.jobs = data.length;
            var count = 0;
            data.forEach(element => {
              if (element.isActive == true) {
                count++;
              }
            });
            this.openjobs = count;
          }
        },
        error => {
        });
  }

  getAllQuestion() {
    this.questionService.getAllQuestions()
      .subscribe(
        data => {
          this.questionlist = data;
          console.log(this.questionlist);
        },
        err => {
          console.log(err);
        }
      )
  }

  getAllscheduler() {
    this.interviewService.getAllInterviews()
      .subscribe(
        response => {
          this.interviewlist = response;
          var int = []
          for (var i = 0; i < this.interviewlist.length; i++) {
            var list = {
              value: this.interviewlist[i].email,
              id: this.interviewlist[i]._id,
              title: this.interviewlist[i].candidatename + ' ' + this.interviewlist[i].jobName,
              start: new Date(this.interviewlist[i].createdDateTime),
              end: new Date(this.interviewlist[i].createdDateTime),
            }
            int.push(list);
          }
          this.events = int;
        },
        err => {
        }
      )
  }
  
  getAllInterviewsKanban() {
    // this.ng4LoadingSpinnerService.show();
    this.interviewService.getAllInterviews()
      .subscribe(
        data => {
          var result = data;

          if (result.length > 0) {
            var cards = [];
            result.forEach(element => {
              if (element.isActive == "p") {
                var card = { id: element._id, "name": element.candidatename, column: "1", text: element.jobName, barColor: "red" };
                cards.push(card);
              }

              if (element.isActive == "i") {
                var card = { id: element._id, "name": element.candidatename, column: "2", text: element.jobName, barColor: "purple" };
                cards.push(card);
              }
              if (element.isActive == "c") {
                var card = { id: element._id, "name": element.candidatename, column: "3", text: element.jobName, barColor: "green" };
                cards.push(card);
              }
              if (element.isActive == "h") {
                var card = { id: element._id, "name": element.candidatename, column: "4", text: element.jobName, barColor: "maroon" };
                cards.push(card);
              }
              if (element.isActive == "r") {
                var card = { id: element._id, "name": element.candidatename, column: "5", text: element.jobName, barColor: "grey" };
                cards.push(card);
              }
            });
            this.config.cards = cards;
            // console.log("cards :- "+cards);
            return cards;

          } else {
            return result;
          }
        },
        error => {
          // this.loading = false;
        });
  }

  getKanbanData() {
    this.config = {
      cards: this.getAllInterviewsKanban(),
      onCardMoved: function (args) {
        this.message("Card moved");
        var self = this;
        // var cards = [];
        // if(args.column.data.id=="1")
        // {
        //   alert ("red")
        //   var card = { barColor: "red"};
        //           cards.push(card);
        // }
        // if(args.column.data.id=="2")
        // {
        //   var card = { barColor: "#F6B26B"};
        //           cards.push(card);
        // }
        // if(args.column.data.id=="3")
        // {
        //   var card = { barColor: "#6AA84F"};
        //           cards.push(card);
        // }
        // this.config.cards=cards;
        // $.post(appConfig.apiUrl + '/interview/updateInterview', {

        //   card: alert(args.card.data.id),
        //   column:alert( args.column.data.id),
        //   position: alert( args.position),

        //   barColor:alert(args.barColor)

        // })
        // alert( args.column.data.id);
        var interview: any = {};
        if (args.column.data.id == 1) {
          args.card.data.barColor = "red";
          interview.isActive = "p";
          interview.barColor = "red";
          interview._id = args.card.data.id;

        } else if (args.column.data.id == 2) {
          args.card.data.barColor = "purple";
          interview.isActive = "i";
          interview.barColor = "purple";
          interview._id = args.card.data.id;
        } else if (args.column.data.id == 3) {
          args.card.data.barColor = "green";
          interview.isActive = "c";
          interview.barColor = "green";
          interview._id = args.card.data.id;
        } else if (args.column.data.id == 4) {
          args.card.data.barColor = "maroon";
          interview.isActive = "h";
          interview.barColor = "maroon";
          interview._id = args.card.data.id;
        } else if (args.column.data.id == 5) {
          args.card.data.barColor = "grey"
          interview.isActive = "r";
          interview.barColor = "grey";
          interview._id = args.card.data.id;
        }

        $.post(appConfig.apiUrl + '/interview/updateKanbanInterview', interview)
          .done(function (data) {
            // alert( "Data Loaded: " + data );
            //self.getAllInterviewsKanban();
          });

        // this.interviewService.update(interview)
        // .subscribe(
        //   data => {
        //     this.getAllInterviewsKanban();
        //   },
        //   error => {
        //     //this.alertService.error(error);
        //     this.loading = false;
        // });

      },
      // ...
    }
  }

  createQuestion(question) {
    this.questionService.createQuestion(question)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Question added successfully');
          this.questionObj = {}
        },
        error => {
          this.loading = false;
        });

  }

  addNewJob() {
    this.jobService.addNewJob(this.job)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Job added successfully');
          this.job = {}
        },
        error => {
          this.loading = false;
        });
  }

  addNewInterview() {
    this.interviewService.addNewInterview(this.interview)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Email send successfully'); 
          this.interview = {}
          console.log(data);
        },
        error => {
          this.toasterService.pop('error', 'Email send Failed'); 
          this.loading = false;
          console.log(error);
        });
  }

  addMultipleInterviews() {
    var datetimepicker = $("#m_datepicker_1").val();
    this.interview.expdate = datetimepicker;
    this.interviewService.addMultipleInterviews(this.interview)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Email send successfully');
          this.interview = {}
        },
        error => {
          this.loading = false;
        });
  }

  updateJob() {
    this.jobService.updateJob(this.job)
      .subscribe(
        data => {
          this.getAllJobs();
        },
        error => {
        });
  }

  copyMessage(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  createdateModal(job) {
    this.job = job;
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 3;
    var year = date.getFullYear();
    var nDate = new Date(year, month, day);
    this.job.expdate = this.datePipe.transform(nDate, 'MM/dd/yyyy');
  }

  openInterviewModal() {
    var date = new Date();
    var day = date.getDate() + 2;
    var month = date.getMonth();
    var year = date.getFullYear();
    var nDate = new Date(year, month, day);
    this.interview.expdate = this.datePipe.transform(nDate, 'MM/dd/yyyy');
  }

  openQuestionModal(question) {
    this.questionObj = question;
  }

  addNewQuestionModal() {
    this.questionObj = {};
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }

  jobSelected(int){
    console.log(int);
    var list = [];
    this.questionlist.forEach(element => {
      if(int.jobID == element.jobID){
        list.push(element.question);
      }
    });
    if(list.length > 0){
      console.log('question added');
      //this.questionlist = list;
      this.job.questions = list;
    } 
  }

  removeAnswer(question, index) {
    const control = <FormArray>question.get('questionList');
    control.removeAt(index);
  }

  onAdd($event) {
    this.questions.push($event);
  }

  onRemove($event) {
    this.questions.splice($event, 1);
  }

  crossplus() {
    this.ap = !this.ap;
  }
}
