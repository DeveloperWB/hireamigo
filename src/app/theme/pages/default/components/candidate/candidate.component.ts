import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { CandidateService } from '../../../../../auth/_services/candidate.service';
import { FileUploader } from 'ng2-file-upload';
import { appConfig } from '../../../../../../app.config';
const TOKEN = localStorage.getItem('token');
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';

let URLDp=appConfig.apiUrl + 'fileupload/';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType, JsonpClientBackend} from '@angular/common/http';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})

export class CandidateComponent implements OnInit, AfterViewInit {

	public uploadfile: FileUploader;
  candidates: any = [];
  public formFlag = false;
  candidateProfileFlag = false;
  public successModal;
  public filterQuery = '';
  loading = false;
  public data;
  candidate: any = {};
  genders = ['Male', 'Female'];
  userType;
  percentDone: number;
  uploadSuccess: boolean;
  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
  constructor(private _script: ScriptLoaderService,
    public candidateService: CandidateService,
    public _router: Router,
    private http: HttpClient,
    toasterService: ToasterService
    ) {
      this.toasterService = toasterService;
      this.userType= localStorage.getItem('userType');
    this.getAllCandidate();
  }

  ngOnInit() {
  //  this.uploadfile.authToken = JSON.parse(TOKEN);
    //this.uploadfile.onAfterAddingFile = (file) => { file.withCredentials = false; };
    // this.uploadfile.onCompleteItem = (item: any, response: any, status: any, headers: any) => {

    // 	if (status == 200){	
    // 		alert('File uploaded....');
    // 		// this.getVideosByUserId(this.user._id);
    // 	}else{
    // 		alert('File not uploaded....');
    // 	}
    // };
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  
  upload(files: File[], candidate){
    //pick from one of the 4 styles of file uploads below
    this.uploadAndProgress(files, candidate);
  }

  uploadAndProgress(files: File[], candidate){
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f));
    
    this.http.post(appConfig.apiUrl+'/candidate/fileupload/'+candidate._id, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
    });
  }

  getAllCandidate() {
    this.candidateService.getAllCandidate()
      .subscribe(
      data => {
        this.candidates = data;
      },
      error => {
        this.loading = false;
      });
  }

  updateCandidate() {
    this.candidateService.updateCandidate(this.candidate)
      .subscribe(
      data => {
        this.getAllCandidate();
      },
      error => {
      });
  }

  addNewCandidateModal() {
    this.candidate = {};
  }

  openCandidateModal(candidate) {
    this.candidate = candidate;
  }

  deleteCandidate(candidate) {
    this.candidateService.deleteCandidate(candidate)
      .subscribe(
      data => {
        this.getAllCandidate();
      },
      error => {
        this.loading = false;
      });
  }

  addNewCandidate() {
    this.candidateService.addNewCandidate(this.candidate)
      .subscribe(
      data => {
        this.toasterService.pop('success', 'Candidate add successfully');
        this.candidate = {};
        this.getAllCandidate();
      },
      error => {
        this.loading = false;

      });
  }

  ngAfterViewInit() {
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }

}
