import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CandidateComponent } from './candidate.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { FileUploadModule } from 'ng2-file-upload';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": CandidateComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes), LayoutModule, FormsModule,
    DataTableModule, HttpModule,
    FileUploadModule,
    ToasterModule

  ], exports: [
    RouterModule
  ], declarations: [
    CandidateComponent, DataFilterPipe
  ]
})
export class CandidateModule { }
