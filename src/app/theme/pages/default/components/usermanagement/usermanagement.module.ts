import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { ProfileupdateComponent } from './profileupdate/profileupdate.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ManageuserComponent, ProfileupdateComponent]
})
export class UsermanagementModule { }
