import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileupdateComponent } from './profileupdate.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../../default.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { FormsModule, Validator } from '@angular/forms';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": ProfileupdateComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FormsModule
  ], exports: [
    RouterModule
  ], declarations: [
    ProfileupdateComponent
  ]
})
export class ProfileupdateModule { }
