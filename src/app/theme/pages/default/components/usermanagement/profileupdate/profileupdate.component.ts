import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../../auth/_services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-profileupdate',
  templateUrl: './profileupdate.component.html',
  styles: []
})
export class ProfileupdateComponent implements OnInit {

  public data;
  user: any = {};
  returnUrl: string;
  loading = false;
  public formFlag = false;

  constructor(private http: HttpClient,
    public userService: UserService,
    public _router: Router) {
    this.getAllUsers();
  }

  ngOnInit() {
  }


  updateUser() {
    this.userService.updateUser(this.user)
      .subscribe(
      data => {
        console.log('Update success..!' + JSON.stringify(data));
        this.getAllUsers();
      },
      error => {
        //this.alertService.error(error);
        this.loading = false;
      });
  }

  deleteUser(user) {

    this.userService.deleteUser(user)
      .subscribe(
      data => {
        console.log('User delete success..!');
        this.getAllUsers();
      },
      error => {
        //this.alertService.error(error);
        this.loading = false;
      });
  }


  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(
      data => {
        this.data = data;
      },
      error => {
        this.loading = false;
      });
  }
}
