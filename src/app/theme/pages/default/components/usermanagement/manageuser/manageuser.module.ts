import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageuserComponent } from './manageuser.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../../default.component';
import { LayoutModule } from '../../../../../layouts/layout.module';
import { FormsModule, Validator } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { UserEqualValidator } from '../../../../../../auth/_services/userPasswordValidator';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';
import { FileUploadModule } from 'ng2-file-upload';

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": ManageuserComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule, FileUploadModule, FormsModule, DataTableModule, HttpModule, ToasterModule
  ], exports: [
    RouterModule
  ], declarations: [
    ManageuserComponent, DataFilterPipe, UserEqualValidator
  ]
})
export class ManageuserModule { }
