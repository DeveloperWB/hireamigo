import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';
import { UserService } from '../../../../../../auth/_services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { FileUploader } from 'ng2-file-upload';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';
import { appConfig } from '../../../../../../../app.config';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType, JsonpClientBackend} from '@angular/common/http';


let URLDp=appConfig.apiUrl + 'fileupload/';

@Component({
  selector: 'app-manageuser',
  templateUrl: './manageuser.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class ManageuserComponent implements OnInit, AfterViewInit {

  public uploadfile: FileUploader;
  public filterQuery = '';
  public data;
  users: any = [];
  user: any =[];
  returnUrl: string;
  loggedInUser : any;
  loading = false;
  public successModal;
  public formFlag = false;
  uploadSuccess: boolean;
  percentDone: number;
  userProfileFlag = false;
  countries = ['India','US'];
  types = ['Admin', 'Reviewer', 'User', 'Super Admin'];
  usertypes = [{ name: 'Manager', value: 2 }, { name: 'Team Lead', value: 3 }];
  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
  constructor(private _script: ScriptLoaderService,
    public userService: UserService,
    public _router: Router,
    toasterService: ToasterService,
    private http: HttpClient,) {
      this.toasterService = toasterService;
    this.getLoggedInUser();
    this.getAllUsers();
  }

  upload(files: File[], user){
    this.uploadAndProgress(files, user);
  }

  uploadAndProgress(files: File[], user){
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f));
    
    this.http.post(appConfig.apiUrl+'/user/fileupload/'+user._id, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
    });
  }

  getLoggedInUser(){
    this.userService.getUser()
    .subscribe(
        data => {
          this.loggedInUser = data.authData.user;
        },
        error => {            
        });
  }

  ngOnInit() {
  }

  
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  
  addNewUserModal() {
    this.user = {};
  }

  createUser() {
    // this.user.userType = 2;
    this.userService.createUser(this.user)
      .subscribe(
      data => {
        this.toasterService.pop('success', 'User add successfully');
        this.user = {};
        this.getAllUsers();
      },
      error => {
        this.loading = false;
      });
  }

  // createUser(){
  //   this.user.userType = 1;
  //   this.user.userID = this.user.email;
  //   if (this.loggedInUser.userType == 2){
  //     this.user.clientId = this.loggedInUser._id;
  //   }
  //   this.userService.createUser(this.user)
  //   .subscribe(
  //       data => {
  //           this.getAllUsers();
  //       },
  //       error => {
  //           this.loading = false;
  //       });
  // }


  updateUser() {
    this.userService.updateUser(this.user)
      .subscribe(
      data => {
        this.getAllUsers();
      },
      error => {
        this.loading = false;
      });
  }

  deleteUser(user) {
    this.userService.deleteUser(user)
      .subscribe(
      data => {
        this.getAllUsers();
      },
      error => {
        this.loading = false;
      });
  }

  openUserModal(user) {
    this.user = user;
  }
  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(
      data => {
        this.users = data;
      },
      error => {
        this.loading = false;
      });
  }

  createNewUserFormOpen() {
    this.userProfileFlag = !this.userProfileFlag;
  }

  ngAfterViewInit() {
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }



}
