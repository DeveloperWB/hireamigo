import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SttComponent } from './stt.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { FormsModule, Validator } from '@angular/forms';
import { ReactiveFormsModule, } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { SelectModule } from 'ng-select';
import { NgSelectModule } from '@ng-select/ng-select';
import { DatePipe } from '@angular/common';

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": SttComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule,
     FormsModule, ReactiveFormsModule,
      DataTableModule, HttpModule, NgSelectModule
  ], exports: [
    RouterModule
  ], declarations: [
    SttComponent
  ],
  providers: [DatePipe] 
})
export class SttModule { }
