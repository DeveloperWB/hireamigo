import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../../_services/script-loader.service';

@Component({
  selector: 'app-actionlog',
  templateUrl: './actionlog.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class ActionlogComponent implements OnInit, AfterViewInit {

  userProfileFlag = true;
  users: {};
  constructor(private _script: ScriptLoaderService) { }

  ngOnInit() {
  }
  ngAfterViewInit() {


    this._script.loadScripts('app-actionlog',
      ['assets/demo/default/custom/components/forms/widgets/bootstrap-daterangepicker.js']);

    this._script.loadScripts('app-actionlog',
      ['assets/demo/default/custom/components/datatables/base/data-local.js']);
  }
  setDataTable() {

    const table: any = $('.m_datatable');
    //table.mDatatable({});
    let datatable = table.mDatatable({
      // datasource definition
      data: {
        type: 'local',
        source: this.users,
        pageSize: 10
      },

      // layout definition
      layout: {
        theme: 'default', // datatable theme
        class: '', // custom wrapper class
        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
        // height: 450, // datatable's body's fixed height
        footer: false // display/hide footer
      },

      // column sorting
      sortable: true,

      pagination: true,

      search: {
        input: $('#generalSearch')
      },

      // inline and bactch editing(cooming soon)
      // editable: false,

      // columns definition
      columns: [{
        field: "_id",
        title: "#",
        width: 10,
        sortable: false,
        textAlign: 'center',
        selector: { class: 'm-checkbox--solid m-checkbox--brand' }
      }, {
        field: "fname",
        title: "FIRST NAME",
        width: 60
      }, {
        field: "lname",
        title: "LAST NAME",
        width: 60
      }, {
        field: "email",
        title: "EMAIL ID",
        width: 100
      },
      // {
      //     field: "expiryDate",
      //     title: "Expiry Date",
      //     type: "date",
      //     format: "MM/DD/YYYY"
      // },
      {
        field: "Actions",
        width: 100,
        title: "Actions",
        sortable: false,
        overflow: 'visible',
        template: function(row, index, datatable) {
          var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';

          return '\
                    <div class="dropdown ' + dropup + '">\
                        <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">\
                            <i class="la la-ellipsis-h"></i>\
                        </a>\
                          <div class="dropdown-menu dropdown-menu-right">\
                            <a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>\
                            <a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>\
                            <a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>\
                          </div>\
                    </div>\
                    <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="View>\
                        <i class="la la-edit"></i>\
                    </a>\
                ';
        }
      }]
    });
    console.log(this.users);
    var query = datatable.getDataSourceQuery();


  }
}

