import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionlogComponent } from './actionlog/actionlog.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ActionlogComponent]
})
export class SettingsModule { }
