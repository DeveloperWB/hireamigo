import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewresponsesComponent } from './viewresponses.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": ViewresponsesComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule
  ], exports: [
    RouterModule
  ], declarations: [
    ViewresponsesComponent
  ]
})
export class ViewresponsesModule { }
