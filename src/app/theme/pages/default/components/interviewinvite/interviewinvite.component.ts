import { Component, OnInit, ViewChild, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { InterviewService } from '../../../../../auth/_services/interview.service';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { JobService } from '../../../../../auth/_services/job.service';
import { IOption } from 'ng-select';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { CandidateService } from '../../../../../auth/_services/candidate.service';
import { FileUploader } from 'ng2-file-upload';
import { appConfig } from '../../../../../../app.config';
import { DatePipe } from '@angular/common';
const TOKEN = localStorage.getItem('token');
let URLfile = appConfig.apiUrl + '';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DayPilotKanbanComponent } from 'daypilot-pro-angular';
import { DataService } from './data.service'; import { Column } from 'primeng/primeng';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';
{ }




@Component({
  selector: 'app-interviewinvite',
  templateUrl: './interviewinvite.component.html',
  // template: `<daypilot-kanban [config]="config" #kanban></daypilot-kanban>`,
  encapsulation: ViewEncapsulation.None,
  styles: ['./interviewinvite.component.css']
})

export class InterviewinviteComponent implements OnInit, AfterViewInit {

  @ViewChild("kanban")
  kanban: DayPilotKanbanComponent;
  public data;
  interview: any = {};
  returnUrl: string;
  loading = false;
  public formFlag = false;
  userProfileFlag = false;
  interviews: any[];
  public joblist;
  public candidatelist;
  public candidateemaillist;
  public uploadfile: FileUploader;

  interviewFlag = true;
  kanbanFlag = false;

  // jobs = ['Doctor', 'Software Engineer', 'Marketing', 'Hr', 'Project Manager'];
  countries = ['India', 'US', 'Uk'];
  status = ['a', 'b', 'c'];
  modal;
  public filterQuery = '';
  public jobs: Array<IOption> = [];
  // public cnames: Array<IOption> = [];
  cnames = [];
  public cemail: Array<IOption> = [];
  userType;
  config: any = [];

  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
  
  constructor(private _script: ScriptLoaderService,
    public interviewService: InterviewService,
    public candidateService: CandidateService,
    public jobService: JobService,
    public _router: Router,
    private http: Http,
    private datePipe: DatePipe,
    private ng4LoadingSpinnerService: Ng4LoadingSpinnerService,
    private ds: DataService,
    toasterService: ToasterService) {
    this.toasterService = toasterService;
    this.userType = localStorage.getItem('userType');
    this.getAllInterviews();
    this.getAllJobs();
    this.getAllCandidate();
    this.ds.getColumns().subscribe(result => {
    this.config.columns = result;
    });
    this.getKanbanData();

  }


  showinterview() {
    this.interviewFlag = true;
    this.kanbanFlag = false;
  }

  showkanban() {
    this.interviewFlag = false;
    this.kanbanFlag = true;
  }


  ngOnInit() {
  }
  candidateEmails = [];

  onAdd($event) {
    this.candidateEmails.push($event);
  }

  onRemove($event) {
    this.candidateEmails.splice($event, 1);
  }

  addNewInterview() {
    this.interviewService.addNewInterview(this.interview)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Email send successfully');
            this.getAllInterviews();
            this.getAllInterviewsKanban();
        },
        error => {
          this.loading = false;
        });
  }

  addMultipleInterviews() {
    this.ng4LoadingSpinnerService.show();
    var datetimepicker = $("#m_datepicker_1").val();
    this.interview.expdate = datetimepicker;
    this.interviewService.addMultipleInterviews(this.interview)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Email send successfully');
          setTimeout(function () {
            this.ng4LoadingSpinnerService.hide();
            this.getAllInterviews();
          }.bind(this), data);
        },
        error => {
          this.loading = false;
        });
  }


  update(interview) {
    this.interviewService.update(interview)
      .subscribe(
        data => {
          this.getAllInterviews();
        },
        error => {
          //this.alertService.error(error);
          this.loading = false;
        });
  }

  deleteInterview(interview) {
    this.interviewService.deleteInterview(interview)
      .subscribe(
        data => {
          this.getAllInterviews();
        },
        error => {
          this.loading = false;
        });
  }

  openInterviewModal() {
    var date = new Date();
    var day = date.getDate() + 2;
    var month = date.getMonth();
    var year = date.getFullYear();
    var nDate = new Date(year, month, day);
    this.interview.expdate = this.datePipe.transform(nDate, 'MM/dd/yyyy');
  }


  getAllInterviewsKanban() {
    // this.ng4LoadingSpinnerService.show();
    this.interviewService.getAllInterviews()
      .subscribe(
        data => {
          var result = data;

          if (result.length > 0) {
            var cards = [];
            result.forEach(element => {
              if (element.isActive == "p") {
                var card = { id: element._id, "name": element.candidatename, column: "1", text: element.jobName, barColor: "red" };
                cards.push(card);
              }

              if (element.isActive == "i") {
                var card = { id: element._id, "name": element.candidatename, column: "2", text: element.jobName, barColor: "purple" };
                cards.push(card);
              }
              if (element.isActive == "c") {
                var card = { id: element._id, "name": element.candidatename, column: "3", text: element.jobName, barColor: "green" };
                cards.push(card);
              }
              if (element.isActive == "h") {
                var card = { id: element._id, "name": element.candidatename, column: "4", text: element.jobName, barColor: "maroon" };
                cards.push(card);
              }
              if (element.isActive == "r") {
                var card = { id: element._id, "name": element.candidatename, column: "5", text: element.jobName, barColor: "grey" };
                cards.push(card);
              }
            });
            this.config.cards = cards;
            // console.log("cards :- "+cards);
            return cards;

          } else {
            return result;
          }
        },
        error => {
          this.loading = false;
        });
  }

  getAllInterviews() {
    this.interviewService.getAllInterviews()
      .subscribe(
        data => {
          var result = data;
          if (result.length > 0) {
            var nData = [];   
            result.forEach(element => {
              if (element.videoPath != null) {
                var path = element.videoPath;
                var fullpath = "http://158.69.63.246:8080/HireAmigo/" + path;
                element.videoPath = fullpath;
                nData.push(element);
              } else {
                nData.push(element);
              }
              this.interviews = nData;
            });      
          } else {
            this.interviews = [];
          }
        },
        error => {
          this.loading = false;
        });
  }

  getAllJobs() {
    this.jobService.getAllJobs()
      .subscribe(
        response => {
          this.joblist = response;
        },
        err => {
          console.log(err);
        }
      )
  }

  getAllCandidate() {
    this.candidateService.getAllCandidate()
      .subscribe(
        response => {
          this.candidatelist = response;
        },
        err => {
          console.log(err);
        }
      )
  }

  createNewUserFormOpen() {
    this.userProfileFlag = !this.userProfileFlag;
  }

  ngAfterViewInit(): void {


    this._script.loadScripts('app-interviewinvite',
      ['assets/demo/default/custom/components/forms/validation/form-widgets.js']);

    this._script.loadScripts('app-interviewinvite',
      ['assets/demo/default/custom/components/base/toastr.js']);

    this._script.loadScripts('app-interviewinvite',
      ['assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js']);

    this._script.loadScripts('app-interviewinvite',
      ['assets/js/live.js']);

    var src = $('#remoteVideo').attr('src');

    // when object with class open-popup is clicked...
    $('.open-popup').click(function (e) {
      e.preventDefault();
      // change the src value of the video
      $('#remoteVideo').attr('src', src);
      $('.popup-bg').fadeIn();
    });

    // when object with class close-popup is clicked...
    $('.close').click(function (e) {
      e.preventDefault();
      $('#remoteVideo').attr('src', '');
      $('.popup-bg').fadeOut();
    });

    //   let self=this;
    // $("showInterview").off().on('click', function() { 
    //   self.showVideo(this.id); 
    // });


  }

  showLive(interview) {
    this.interview = interview;
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }


  getKanbanData() {
    this.config = {
      cards: this.getAllInterviewsKanban(),
      onCardMoved: function (args) {
        this.message("Card moved");
        var self=this;
        // var cards = [];
        // if(args.column.data.id=="1")
        // {
        //   alert ("red")
        //   var card = { barColor: "red"};
        //           cards.push(card);
        // }
        // if(args.column.data.id=="2")
        // {
        //   var card = { barColor: "#F6B26B"};
        //           cards.push(card);
        // }
        // if(args.column.data.id=="3")
        // {
        //   var card = { barColor: "#6AA84F"};
        //           cards.push(card);
        // }
        // this.config.cards=cards;
        // $.post(appConfig.apiUrl + '/interview/updateInterview', {

        //   card: alert(args.card.data.id),
        //   column:alert( args.column.data.id),
        //   position: alert( args.position),

        //   barColor:alert(args.barColor)

        // })
        // alert( args.column.data.id);
        var interview: any = {};
        if (args.column.data.id == 1) {
           args.card.data.barColor = "red";
          interview.isActive = "p";
          interview.barColor = "red";
          interview._id = args.card.data.id;

        } else if (args.column.data.id == 2) {
           args.card.data.barColor = "purple";
          interview.isActive = "i";
          interview.barColor = "purple";
          interview._id = args.card.data.id;
        } else if (args.column.data.id == 3) {
           args.card.data.barColor = "green";
          interview.isActive = "c";
          interview.barColor = "green";
          interview._id = args.card.data.id;
        } else if (args.column.data.id == 4) {
           args.card.data.barColor = "maroon";
          interview.isActive = "h";
          interview.barColor = "maroon";
          interview._id = args.card.data.id;
        } else if (args.column.data.id == 5) {
           args.card.data.barColor = "grey"
          interview.isActive = "r";
          interview.barColor = "grey";
          interview._id = args.card.data.id;
        }

        $.post( appConfig.apiUrl + '/interview/updateKanbanInterview', interview)
        .done(function( data ) {
          // alert( "Data Loaded: " + data );
          //self.getAllInterviewsKanban();
        });       
      
        // this.interviewService.update(interview)
        // .subscribe(
        //   data => {
        //     this.getAllInterviewsKanban();
        //   },
        //   error => {
        //     //this.alertService.error(error);
        //     this.loading = false;
        // });

      },
      // ...
    }
  }
   setInterviewKanban(){
    console.log("alllllllll");
  }



}
