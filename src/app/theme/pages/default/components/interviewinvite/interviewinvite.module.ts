import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewinviteComponent } from './interviewinvite.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { FormsModule, Validator } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { SelectModule } from 'ng-select';
import { NgSelectModule } from '@ng-select/ng-select';
import { FileUploadModule } from 'ng2-file-upload';
import { DatePipe } from '@angular/common';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { DayPilotModule } from 'daypilot-pro-angular';
import { DataService } from './data.service';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": InterviewinviteComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule,DayPilotModule, ToasterModule,
    FormsModule, DataTableModule, HttpModule, NgSelectModule,FileUploadModule,Ng4LoadingSpinnerModule.forRoot(), 
  ], exports: [
    RouterModule
  ], declarations: [
    InterviewinviteComponent, DataFilterPipe   
  ],
  providers: [DatePipe,DataService] 
})
export class InterviewinviteModule { }
