import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobmanagementComponent } from './jobmanagement.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { FormsModule, Validator } from '@angular/forms';
import { ReactiveFormsModule, } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { SelectModule } from 'ng-select';
import { NgSelectModule } from '@ng-select/ng-select';
import { DatePipe } from '@angular/common';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": JobmanagementComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule,
     FormsModule, ReactiveFormsModule,ToasterModule,
      DataTableModule, HttpModule, NgSelectModule
  ], exports: [
    RouterModule
  ], declarations: [
    JobmanagementComponent, DataFilterPipe
  ],
  providers: [DatePipe] 
})
export class JobmanagementModule { }
