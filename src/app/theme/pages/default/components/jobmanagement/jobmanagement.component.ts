import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { JobService } from '../../../../../auth/_services/job.service';
import { QuestionService } from '../../../../../auth/_services/question.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { IOption } from 'ng-select';
import { NgSelectModule, NgOption } from '@ng-select/ng-select';
import { DatePipe } from '@angular/common';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';


@Component({
  selector: 'app-jobmanagement',
  templateUrl: './jobmanagement.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class JobmanagementComponent implements OnInit, AfterViewInit {

  public data;
  loading = false;
  job: any = {};
  public jobstatus;
  userProfileFlag = true;
  questionForm: FormGroup;
  answerForm: FormGroup;
  questionObj: any = {};
  answers: any = [];
  question: any = [];
  jobs: any;

  public departmentlist;
  public domainlist;
  questions : any = [];
  public dept: Array<IOption> = [];
  public dom: Array<IOption> = [];
  public ap: boolean = false;
  // public questionlist: Array<IOption> = [];
  public questionlist;
  departments = ['Technology', 'HR'];
  domains = ['Introductory', 'Personal', 'Behavioral', 'Technical', ' Aptitude'];
  enable;
  JobFlag = false;
  formFlag = false;
  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
  constructor(private _script: ScriptLoaderService,
    private formBuilder: FormBuilder,
    public jobService: JobService,
    public questionService: QuestionService,
    public _router: Router,
    private http: Http,
    private datePipe: DatePipe,
    toasterService: ToasterService) {
      this.toasterService = toasterService;
   // this.createForm();
    this.getAllJobs();
    // this.getAlldepartment();
    this.getAllquestion();
  }


  ngOnInit() {
  }

  crossplus() {
    this.ap = !this.ap;
  }

  onAdd($event) {
    this.questions.push($event);
  }

  onRemove($event) {
    this.questions.splice($event, 1);
  }


  // updateForm(){
  //   this.questionForm = this.formBuilder.group({
  //     questions: this.formBuilder.array([this.updateQuestionForm()])
  //   });
  // }

  // updateQuestionForm(): FormGroup{
  //   return this.formBuilder.group({
  //     department: this.job.department,
  //     domain: this.job.domain,
  //     // question: [''],
  //     questionList: this.formBuilder.array([this.updateAnswerForm()])
  //     //answers: this.formBuilder.array([this.createAnswerForm()])
  //   });
  // }
  // updateAnswerForm(){
  //   return this.formBuilder.group({
  //     question: this.job.questionList
  //   });
  // }

  // createForm() {

  //   this.questionForm = this.formBuilder.group({
  //     questions: this.formBuilder.array([this.createQuestionForm()])
  //   });

  // }

  getAllquestion() {
    this.questionService.getAllQuestions()
      .subscribe(
      response => {
        this.questionlist = response;
        // var int = []
        // for (var i = 0; i < this.questions.length; i++) {
        //   var list = { label: this.questions[i].question, value: this.questions[i].question }
        //   int.push(list);
        // }
        // this.questionlist = int;
        // console.log(this.questionlist);
      },
      err => {
        console.log(err);
      }
      )
  }

  // createQuestionForm(): FormGroup {
  //   return this.formBuilder.group({
  //     // typRadio: [''],
  //     department: [''],
  //     domain: [''],
  //     // question: [''],
  //     questionList: this.formBuilder.array([this.createAnswerForm()])
  //     //answers: this.formBuilder.array([this.createAnswerForm()])
  //   });
  // }

  // createAnswerForm() {
  //   return this.formBuilder.group({
  //     question: ['']
  //   });
  // }

  // addAnswers(answer) {
  //   this.question = answer.get('questionList') as FormArray;
  //   this.question.push(this.createAnswerForm());
  // }

  removeAnswer(question, index) {
    const control = <FormArray>question.get('questionList');
    control.removeAt(index);
  }

  addNewQuestionModal() {
    this.questionObj = {};
  }

  openQuestionModal(question) {
    this.questionObj = question;
  }


    addquestionanswerFormOpen() {
      this.userProfileFlag = !this.userProfileFlag;
    }

  ngAfterViewInit() {
    this._script.loadScripts('app-jobmanagement',
      ['assets/demo/default/custom/components/forms/wizard/wizard.js']);

      this._script.loadScripts('app-jobmanagement',
      ['assets/demo/default/custom/components/forms/validation/form-widgets.js']);

      this._script.loadScripts('app-jobmanagement',
      ['assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js']);
 }

  // addQuestion() {
  //   this.questions = this.questionForm.get('questions') as FormArray;
  //   this.questions.push(this.createQuestionForm());
  // }

  addNewjobModal() {
    this.job = {};
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 3;
    var year = date.getFullYear();
    var nDate = new Date(year, month, day);
    this.job.expdate = this.datePipe.transform(nDate, 'MM/dd/yyyy');
  }

  openJobModal(job) {
    this.job = job;
    var date = new Date();
    var day = date.getDate();
    var month = date.getMonth() + 3;
    var year = date.getFullYear();
    var nDate = new Date(year, month, day);
    this.job.expdate = this.datePipe.transform(nDate, 'MM/dd/yyyy');
  }


  addNewJob() {
    this.jobService.addNewJob(this.job)
      .subscribe(
      data => {
        this.toasterService.pop('success', 'Job add successfully');
        this.JobFlag = false;
        this.getAllJobs();
      },
      error => {
        this.loading = false;
      });
  }

  activeInactiveJob(job){
    this.jobService.activeInactiveJob(job)
    .subscribe(
        data => {
            this.getAllJobs();
        },
        error => {
            this.loading = false;
        });
  }


  updateJob() {
    this.jobService.updateJob(this.job)
      .subscribe(
      data => {
        this.getAllJobs();
      },
      error => {
      });
  }

  deleteJob(job) {
    this.jobService.deleteJob(job)
      .subscribe(
      data => {
        this.getAllJobs();
      },
      error => {
        this.loading = false;
      });
  }

  getAllJobs() {
    this.jobService.getAllJobs()
      .subscribe(
      data => {
        this.jobs = data;
      },
      error => {
        this.loading = false;
      });
  }

  openstatus(){

    this.jobService.openstatus(this.enable)
    .subscribe(
      response=>{

      if(this.enable==1)
      {
        this.jobs=response.statusList;

      }
      else
      {
        this.jobs=response.statusList;
      }
      },
        err=> {
          console.log(err);
        }
    )
  }

  closestatus(){
    this.jobService.closestatus(this.enable)
    .subscribe(
      response=>{

      if(this.enable==1)
      {
        this.jobs=response.statusList;

      }
      else
      {
        this.jobs=response.statusList;
      }
      },
        err=> {
          console.log(err);
        }
    )
  }

  allstatus(){
    this.jobService.allstatus()
    .subscribe(
      response=>{

      if(this.enable==1)
      {
        this.jobs=response.statusList;

      }
      else
      {
        this.jobs=response.statusList;
      }
      },
        err=> {
          console.log(err);
        }
    )
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }

  // getAlldepartment() {
  //   this.jobService.getAlldepartment()
  //     .subscribe(
  //     response => {
  //       console.log(response);
  //       this.departmentlist = response;
  //       var int = []
  //       for (var i = 0; i < this.departmentlist.length; i++) {
  //         var list = { label: this.departmentlist[i].department, value: this.departmentlist[i].department }
  //         int.push(list);
  //       }
  //       this.dept = int;
  //       console.log(this.dept);
  //     },
  //     err => {
  //       console.log(err);
  //     }
  //     )
  // }

  addMultipleQuestion() {
    var question = this.questionForm.value.questions[0];
    this.questionService.addMultipleQuestion(question)
      .subscribe(
      data => {
        this.job.questionId = data.job._id;
        question = {};
      },
      error => {
        this.loading = false;
      });
  }

  createQuestion(question) {
    this.questionService.createQuestion(question)
      .subscribe(
      data => {
        this.getAllquestion();
        //this.job.questionId = data.job._id;
        //question = {};
      },
      error => {
        this.loading = false;
      });

  }

  deleteQuestion(question) {
    this.questionService.deleteQuestion(question)
      .subscribe(
      data => {
      },
      error => {
        this.loading = false;
      });
  }

}
