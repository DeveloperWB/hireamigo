import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientmanagementComponent } from './clientmanagement.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { ClientEqualValidator } from '../../../../../auth/_services/clientPasswordValidator';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';
import { FileUploadModule } from 'ng2-file-upload';



const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": ClientmanagementComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), FileUploadModule, LayoutModule, FormsModule, DataTableModule, HttpModule,ToasterModule
  ], exports: [
    RouterModule
  ], declarations: [
    ClientmanagementComponent, DataFilterPipe, ClientEqualValidator
  ]
})
export class ClientmanagementModule { }
