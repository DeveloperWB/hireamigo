import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { ClientService } from '../../../../../auth/_services/client.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { UserService } from '../../../../../auth/_services/index.1';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';
import { FileUploader } from 'ng2-file-upload';
import { appConfig } from '../../../../../../app.config';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType, JsonpClientBackend} from '@angular/common/http';


@Component({
  selector: 'app-clientmanagement',
  templateUrl: './clientmanagement.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class ClientmanagementComponent implements OnInit, AfterViewInit {

  public uploadfile: FileUploader;
  clients: any = [];
  public formFlag = false;
  uploadSuccess: boolean;
  percentDone: number
  clientProfileFlag = false;
  public successModal;
  public filterQuery = '';
  loading = false;
  public data;
  client: any = {};
  user: any = [];
  users: any = [];
  usertypes = [{ name: 'Manager', value: 2 }, { name: 'Team Lead', value: 3 }];


  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
  constructor(private _script: ScriptLoaderService,
    private clientService: ClientService,
    public _router: Router,
    public userService: UserService,
    private http: HttpClient,
    toasterService: ToasterService) {
    this.getAllClients();
    //this.getAllUsers();
    this.toasterService = toasterService;
  }

  ngOnInit() {
  }

  upload(files: File[], client){
    this.uploadAndProgress(files, client);
  }

  uploadAndProgress(files: File[], client){
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f));
    
    this.http.post(appConfig.apiUrl+'/client/fileupload/'+ client._id, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
    });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  
  getAllClients() {
    this.clientService.getAllClients()
      .subscribe(
        data => {
          this.clients = data;
        },
        error => {
          this.loading = false;
        });
  }

  updateClient() {
    this.clientService.updateClient(this.client)
      .subscribe(
        data => {
          this.getAllClients();
        },
        error => {
          this.loading = false;
        });
  }

  addNewClientModal() {
    this.client = {};
    this.client.companyId = Math.floor(1000 + Math.random() * 9000);
    console.log(this.client.companyId);
  }

  openClientModal(client) {
    this.client = client;
  }

  deleteClient(client) {
    this.clientService.deleteClient(client)
      .subscribe(
        data => {
          this.getAllClients();
        },
        error => {
          this.loading = false;
        });
  }

  addNewClient() {
    this.client.userType = 1;
    this.clientService.addNewClient(this.client)
      .subscribe(
        data => {
          this.toasterService.pop('success', 'Client add successfully');
          this.client = {};
          this.getAllClients();
        },
        error => {
          this.loading = false;
        });
  }

  ngAfterViewInit() {
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }


  // createUser() {
  //   // this.client.userType = 1;
  //   this.userService.createUser(this.client)
  //     .subscribe(
  //     data => {
  //       console.log('Client Create success..!');
  //       this.client = {};
  //       this.getAllUsers();
  //     },
  //     error => {
  //       this.loading = false;
  //     });
  // }

  // getAllUsers() {
  //   this.userService.getAllUsers()
  //     .subscribe(
  //     data => {
  //       this.clients = data;
  //     },
  //     error => {
  //       this.loading = false;
  //     });
  // }

  // deleteUser(client) {
  //   this.userService.deleteUser(client)
  //     .subscribe(
  //     data => {
  //       this.getAllUsers();
  //     },
  //     error => {
  //       this.loading = false;
  //     });
  // }

//   updateUser() {
//     this.userService.updateUser(this.client)
//       .subscribe(
//       data => {
//         this.getAllUsers();
//       },
//       error => {
//         this.loading = false;
//       });
//   }
}
