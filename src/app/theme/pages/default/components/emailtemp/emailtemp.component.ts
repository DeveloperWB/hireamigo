import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType, JsonpClientBackend} from '@angular/common/http';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { UserService } from '../../../../../auth/_services/user.service';
import { EmailtempService } from '../../../../../auth/_services/emailtemp.service';
import { ClientService } from '../../../../../auth/_services/client.service';
import { IOption } from 'ng-select';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';


@Component({
  selector: 'app-emailtemp',
  templateUrl: './emailtemp.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.None,
})
export class EmailtempComponent implements OnInit, AfterViewInit {
  public Editor = ClassicEditor;
  public clientlist;
  public filterQuery = '';
  public data;
  loading = false;
  public formFlag = false;
  emailtemps: any = [];
  emailtemp: any = {};
  
  public clientData: Array<IOption> = [];

  constructor(private _script: ScriptLoaderService,
    public emailtempService: EmailtempService,
    private clientService: ClientService) {
      this.getAllTemplate();
      this.getAllClients();

  }


  getAllTemplate() {
    this.emailtempService.getAllTemplate()
      .subscribe(
      data => {
        this.emailtemps = data;
      },
      error => {
        this.loading = false;
      });
  }

  updateEmailtemp() {
    this.emailtempService.updateEmailtemp(this.emailtemp)
      .subscribe(
      data => {
        this.getAllTemplate();
      },
      error => {
      });
  }

  delete(emailtemp) {
    this.emailtempService.delete(emailtemp)
      .subscribe(
      data => {
        this.getAllTemplate();
      },
      error => {
        this.loading = false;
      });
  }

  addEmailtemp() {
    console.log(this.emailtemp);
    this.emailtempService.addEmailtemp(this.emailtemp)
      .subscribe(
      data => {
        this.getAllTemplate();
        this.emailtemp = {};
      },
      error => {
        this.loading = false;
      });
  }

  ngOnInit() {
  }

  addemailtempModal() {
    this.emailtemp = {};
  }

  openEmailModal(emailtemp) {
    this.emailtemp = emailtemp;
  }

  ngAfterViewInit() {
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }
  
  getAllClients() {
    this.clientService.getAllClients()
      .subscribe(
        data => {
          this.clientlist = data;
 
                var int = []
                for (var i = 0; i < this.clientlist.length; i++) {
                  var list = { label: this.clientlist[i].fname, value: this.clientlist[i]._id }
                  int.push(list);
                }
                this.clientData = int;
        },
        error => {
          this.loading = false;
        });
  }
  
  

}
