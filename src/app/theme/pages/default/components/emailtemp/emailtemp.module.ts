import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailtempComponent } from './emailtemp.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';
import { DataFilterPipe } from './datafilterpipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

const routes: Routes = [
  {
    
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": EmailtempComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, CKEditorModule,NgSelectModule, RouterModule.forChild(routes), LayoutModule, FormsModule, DataTableModule, HttpModule,ToasterModule
  ], exports: [
    RouterModule
  ], declarations: [
    EmailtempComponent, DataFilterPipe
  ]
})
export class EmailtempModule { }
