import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ScriptLoaderService } from '../../../../../_services/script-loader.service';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { QuestionService } from '../../../../../auth/_services/question.service';
import { JobService } from '../../../../../auth/_services/job.service';
import { Http } from '@angular/http';
import { IOption } from 'ng-select';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';



@Component({
  selector: 'app-questionbank',
  templateUrl: './questionbank.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: []
})
export class QuestionbankComponent implements OnInit, AfterViewInit {

  
  loading = false;
  public data;
  user: any = {};
  questionObj: any = {};
  userProfileFlag = true;
  questionForm: FormGroup;
  answerForm: FormGroup;
  public jobList: Object[];
  public formFlag = false;
  questions: any = [];
  answers: any = [];
  openjobs = 0;
  job: any = {};
  jobs = 0;
  question: any = [];
  public departmentlist;
  public domainlist;
  public dept: Array<IOption> = [];
  public dom: Array<IOption> = [];
  public joblist;
  public filterQuery = '';
  departments = ['Technology', 'HR'];
  domains = ['Introductory', 'Personal', 'Behavioral', 'Technical', ' Aptitude'];

  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });

  constructor(private _script: ScriptLoaderService,
    private formBuilder: FormBuilder,
    private jobService: JobService,
    public questionService: QuestionService,
    private http: Http,
    toasterService: ToasterService) {
      this.toasterService = toasterService;
    //this.createForm();
    this.getAllQuestions();
    this.getAllJobs();
    //this.getAlldepartment();
    //this.getAlldomain();

  }
  ngOnInit() {

  }

  addNewQuestionModal() {
    this.questionObj = {};
  }

  openQuestionModal(question) {
    this.questionObj = question;
  }

  createForm() {

    this.questionForm = this.formBuilder.group({
      questions: this.formBuilder.array([this.createQuestionForm()])
    });

  }

  
  createQuestionForm(): FormGroup {
    return this.formBuilder.group({
      department: [''],
      domain: [''],
      // question: [''],
      questionList: this.formBuilder.array([this.createAnswerForm()])
      //answers: this.formBuilder.array([this.createAnswerForm()])
    });
  }

  // getAlldepartment() {
  //   this.questionService.getAlldepartment()
  //     .subscribe(
  //     response => {
  //       console.log(response);
  //       this.departmentlist = response;
  //       var int = []
  //       for (var i = 0; i < this.departmentlist.length; i++) {
  //         var list = { label: this.departmentlist[i].department, value: this.departmentlist[i].department }
  //         int.push(list);
  //       }
  //       this.dept = int;
  //       console.log(this.dept);
  //     },
  //     err => {
  //       console.log(err);
  //     }
  //     )
  // }

  // getAlldomain() {
  //   this.questionService.getAlldomain()
  //     .subscribe(
  //     response => {
  //       console.log(response);
  //       this.domainlist = response;
  //       var int = []
  //       for (var i = 0; i < this.domainlist.length; i++) {
  //         var list = { label: this.domainlist[i].domain, value: this.domainlist[i].domain }
  //         int.push(list);
  //       }
  //       this.dom = int;
  //       console.log(this.dom);
  //     },
  //     err => {
  //       console.log(err);
  //     }
  //     )
  // }

  createAnswerForm() {
    return this.formBuilder.group({
      question: ['']
    });
  }

  addAnswers(answer) {
    this.question = answer.get('questionList') as FormArray;
    this.question.push(this.createAnswerForm());
  }

  removeAnswer(question, index) {
    const control = <FormArray>question.get('questionList');
    control.removeAt(index);
  }

  ngAfterViewInit() {
    this._script.loadScripts('app-questionbank',
      ['assets/demo/default/custom/components/forms/validation/form-widgets.js']);
  }

  addNewQuestion(question) {
    this.questionService.addNewQuestion(question)
      .subscribe(
      data => {
        this.getAllQuestions();
      },
      error => {
        this.loading = false;
      });

  }

  addMultipleQuestion() {
    var question = this.questionForm.value.questions[0];
    this.questionService.addMultipleQuestion(question)
      .subscribe(
      data => {
        this.job.questionId = data.job._id;
        question = {};
      },
      error => {
        this.loading = false;
      });
  }

  editQuestion(question) {
    this.questionService.editQuestion(question)
      .subscribe(
      data => {
        this.getAllQuestions();
      },
      error => {
        this.loading = false;
      });
  }

  updateQuestion() {
    this.questionService.updateQuestion(this.question)
      .subscribe(
      data => {
        this.getAllQuestions();
      },
      error => {
      });
  }

  deleteQuestion(question) {
    this.questionService.deleteQuestion(question)
      .subscribe(
      data => {
        this.getAllQuestions();
      },
      error => {
        this.loading = false;
      });
  }

  getAllQuestions() {
    this.questionService.getAllQuestions()
      .subscribe(
      data => {
        this.questions = data;
      },
      error => {
        this.loading = false;
      });
  }

  createQuestion(question) {
    this.questionService.createQuestion(question)
      .subscribe(
      data => {
        this.toasterService.pop('success', 'Job add successfully');
        this.getAllQuestions();
      },
      error => {
        this.loading = false;
      });
  }

  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.name.length;
  }

  getAllJobs() {
    this.jobService.getAllJobs()
      .subscribe(
        data => {
          // console.log(data);
          this.jobList = data;
          if (data.length > 0) {
            this.jobs = data.length;
            var count = 0;
            data.forEach(element => {
              if (element.isActive == true) {
                count++;
              }
            });
            this.openjobs = count;
          }

        },
        error => {
        });
  }
}
