import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionbankComponent } from './questionbank.component';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from '../../default.component';
import { LayoutModule } from '../../../../layouts/layout.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';
import { HttpModule } from '@angular/http';
import { DataFilterPipe } from './datafilterpipe';
import { NgSelectModule } from '@ng-select/ng-select';
import { IOption } from 'ng-select';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';




const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": QuestionbankComponent
      }
    ]
  }
];
@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), LayoutModule,ToasterModule,
    ReactiveFormsModule, FormsModule, DataTableModule, HttpModule, NgSelectModule
  ], exports: [
    RouterModule
  ], declarations: [
    QuestionbankComponent, DataFilterPipe,
  ]
})
export class QuestionbankModule { }
