import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../../helpers';
import { UserService } from '../../../../../auth/_services/index.1';
import { ToasterModule, ToasterService, ToasterConfig }  from 'angular2-toaster/angular2-toaster';
import { appConfig } from '../../../../../../app.config';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType, JsonpClientBackend} from '@angular/common/http';
import { FileUploader } from 'ng2-file-upload';


@Component({
  selector: "app-header-profile",
  templateUrl: "./header-profile.component.html",
  encapsulation: ViewEncapsulation.None,
})
export class HeaderProfileComponent implements OnInit {

  public uploadfile: FileUploader;
  loading = false;
  user: any =[];
  users: any = [];
  percentDone: number;
  uploadSuccess: boolean;


  countries = ['India','US'];
  usertypes = [{ name: 'Manager', value: 2 }, { name: 'Team Lead', value: 3 }];

  private toasterService: ToasterService;
  public toasterconfig : ToasterConfig =
  new ToasterConfig({
    tapToDismiss: true,
    timeout: 5000
  });
  constructor(private userService: UserService,
    toasterService: ToasterService,
    private http: HttpClient) {
      this.toasterService = toasterService;
  // this.getAllUsers();
  this.getLoggedInUser();
  }
  ngOnInit() {

  }

  getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(
      data => {
        this.users = data;
      },
      error => {
        this.loading = false;
      });
  }


  getLoggedInUser() {
    this.userService.getLoggedInUser()
      .subscribe(
      data => {
            this.user=data.user.user;
            console.log(this.user);
      },
      error => {
        this.loading = false;
      });
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  upload(files: File[], user){
    this.uploadAndProgress(files, user);
  }

  uploadAndProgress(files: File[], user){
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file',f));
    
    this.http.post(appConfig.apiUrl+'/user/fileupload/'+user._id, formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) {
          this.percentDone = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.uploadSuccess = true;
        }
    });
  }
}
