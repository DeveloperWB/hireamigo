import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import { UserService } from '../../../auth/_services/index.1';

declare let mLayout: any;
@Component({
  selector: "app-header-nav",
  templateUrl: "./header-nav.component.html",
  encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {

  user: any =[];
  username: any;
  loading = false;
  constructor(private userService: UserService) {
    this.getLoggedInUser();
  }
  getLoggedInUser() {
    this.userService.getLoggedInUser()
      .subscribe(
      data => {
            this.user=data.user.user;
      },
      error => {
        this.loading = false;
      });
  }
  ngOnInit() {
    this.username = localStorage.getItem('username');
  }
  ngAfterViewInit() {

    mLayout.initHeader();

  }

}
