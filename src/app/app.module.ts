import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ThemeComponent } from './theme/theme.component';
import { LayoutModule } from './theme/layouts/layout.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ScriptLoaderService } from "./_services/script-loader.service";
import { ThemeRoutingModule } from "./theme/theme-routing.module";
import { AuthModule } from "./auth/auth.module";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FileUploadModule } from 'ng2-file-upload';

import { JwtInterceptor } from './auth/_helpers';
import { AuthenticationService, UserService, EmailtempService, ClientService, DataService, JobService, QuestionService, InterviewService, AlertService, CandidateService, } from './auth/_services/index.1';
import { StartInterviewModule } from './auth/start-interview/start-interview.module';
import { ThankyouModule } from './auth/thankyou/thankyou.module';
import { PagenotfoundModule } from './auth/pagenotfound/Pagenotfound.module';
import { RoleGuardService } from './auth/_guards/roleGuardService';
import { ToasterService } from 'angular2-toaster';



@NgModule({
  declarations: [
    ThemeComponent,
    AppComponent,
    
  ],
  imports: [
    LayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ThemeRoutingModule,
    AuthModule,
    StartInterviewModule,
    ThankyouModule,
    HttpClientModule,
    PagenotfoundModule,
    

  ],
  providers: [
    RoleGuardService,
    AuthenticationService,
    UserService,
    ClientService,
    ScriptLoaderService,
    JobService,
    QuestionService,
    InterviewService,
    AlertService,
    CandidateService,
    DataService,
    ToasterService,
    EmailtempService,


    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
